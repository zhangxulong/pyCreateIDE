import sys

import pandas as pd
import pymysql as MySQLdb
from PyQt5 import QtCore
from PyQt5.QtCore import QRegExp, Qt
from PyQt5.QtCore import (QSettings, QByteArray)
from PyQt5.QtGui import QFont, QSyntaxHighlighter, QTextCharFormat
from PyQt5.QtWidgets import QDialog, QLineEdit, QPushButton, QMessageBox, \
    QLabel, QGridLayout, QComboBox, QFileDialog, QTableWidget, QTableWidgetItem, QVBoxLayout
from PyQt5.QtWidgets import (QListWidget,
                             QSplitter, QTextBrowser)
from PyQt5.QtWidgets import QMainWindow, QMenu, QApplication, QDesktopWidget, QTextEdit


class Highlighter(QSyntaxHighlighter):
    def __init__(self, parent=None):
        super(Highlighter, self).__init__(parent)

        keywordFormat = QTextCharFormat()
        keywordFormat.setForeground(Qt.blue)
        keywordFormat.setFontWeight(QFont.Bold)
        # TODO fulfill heilight contend
        keywordPatterns = [
            "DROP", "WHERE", "DATABASE",
            "BACKUP", "UPDATE", "DESC",
            "AVG", "EXCEPT", "TOP", 'ALTER',
            "CONVERT", "TABLE", 'CREATE',
            'SELECT', 'FROM'
            # "\\bDROP\\b", "\\bWHERE\\b", "\\bDATABASE\\b",
            # "\\bBACKUP\\b", "\\bUPDATE\\b", "\\bDESC\\b",
            # "\\bAVG\\b", "\\bEXCEPT\\b", "\\bTOP\\b", '\\bALTER\\b',
            # "\\bCONVERT\\b", "\\bTABLE\\b", '\\bCREATE\\b',
            # '\\bSELECT\\b', '\\bFROM\\b'
        ]

        self.highlightingRules = [(QRegExp(pattern), keywordFormat)
                                  for pattern in keywordPatterns]

        classFormat = QTextCharFormat()
        classFormat.setFontWeight(QFont.Bold)
        classFormat.setForeground(Qt.blue)
        self.highlightingRules.append((QRegExp("\\bQ[A-Za-z]+\\b"),
                                       classFormat))

        singleLineCommentFormat = QTextCharFormat()
        singleLineCommentFormat.setForeground(Qt.red)
        self.highlightingRules.append((QRegExp("//[^\n]*"),
                                       singleLineCommentFormat))

        self.multiLineCommentFormat = QTextCharFormat()
        self.multiLineCommentFormat.setForeground(Qt.red)

        quotationFormat = QTextCharFormat()
        quotationFormat.setForeground(Qt.darkGreen)
        self.highlightingRules.append((QRegExp("\".*\""), quotationFormat))

        functionFormat = QTextCharFormat()
        functionFormat.setFontItalic(True)
        functionFormat.setForeground(Qt.red)
        self.highlightingRules.append((QRegExp("\\b[A-Za-z0-9_]+(?=\\()"),
                                       functionFormat))

        self.commentStartExpression = QRegExp("/\\*")
        self.commentEndExpression = QRegExp("\\*/")

    def highlightBlock(self, text):
        for pattern, format in self.highlightingRules:
            expression = QRegExp(pattern)
            index = expression.indexIn(text)
            while index >= 0:
                length = expression.matchedLength()
                self.setFormat(index, length, format)
                index = expression.indexIn(text, index + length)

        self.setCurrentBlockState(0)

        startIndex = 0
        if self.previousBlockState() != 1:
            startIndex = self.commentStartExpression.indexIn(text)

        while startIndex >= 0:
            endIndex = self.commentEndExpression.indexIn(text, startIndex)

            if endIndex == -1:
                self.setCurrentBlockState(1)
                commentLength = len(text) - startIndex
            else:
                commentLength = endIndex - startIndex + self.commentEndExpression.matchedLength()

            self.setFormat(startIndex, commentLength,
                           self.multiLineCommentFormat)
            startIndex = self.commentStartExpression.indexIn(text,
                                                             startIndex + commentLength)


class ViewDesc(QDialog):
    def __init__(self, parent=None):
        super(ViewDesc, self).__init__(parent)
        self.setWindowTitle('view Desc')
        self.screen = QDesktopWidget().screenGeometry()
        self.setGeometry(self.screen.width() / 4, self.screen.height() / 4, self.screen.width() / 2,
                         self.screen.height() / 2)
        self.data_seg = window.desc_cont
        self.layout = QVBoxLayout()
        self.checked_filed_list = []
        self.tableWidget = QTableWidget()
        # set row count
        self.tableWidget.setRowCount(len(self.data_seg))
        # set column count
        self.tableWidget.setColumnCount(3)
        self.tableWidget.itemClicked.connect(self.handleItemClicked)
        row = 0
        for seg in self.data_seg:
            filedItem = QTableWidgetItem(seg[0])
            filedItem.setFlags(QtCore.Qt.ItemIsUserCheckable |
                               QtCore.Qt.ItemIsEnabled)
            filedItem.setCheckState(QtCore.Qt.Unchecked)
            typeItem = QTableWidgetItem(seg[1])
            # typeItem.setFlags(QtCore.Qt.ItemIsUserCheckable |
            #                   QtCore.Qt.ItemIsEnabled)
            # typeItem.setCheckState(QtCore.Qt.Unchecked)
            keyItem = QTableWidgetItem(str(seg[2]))
            # keyItem.setFlags(QtCore.Qt.ItemIsUserCheckable |
            #                  QtCore.Qt.ItemIsEnabled)
            # keyItem.setCheckState(QtCore.Qt.Unchecked)
            self.tableWidget.setItem(row, 0, filedItem)
            self.tableWidget.setItem(row, 1, typeItem)
            self.tableWidget.setItem(row, 2, keyItem)
            row += 1
        self.buttonSubmit = QPushButton('Submit')
        self.layout.addWidget(self.tableWidget)
        self.layout.addWidget(self.buttonSubmit)
        self.buttonSubmit.clicked.connect(self.replace2editor)
        self.setLayout(self.layout)

    def replace2editor(self):
        # TODO replace *
        currentEditorText = window.editor.toPlainText()
        print(currentEditorText)
        replace_with_str = ''
        for filed in self.checked_filed_list:
            replace_with_str += (filed + ',')
        currentEditorText = currentEditorText.replace('*', replace_with_str)
        window.editor.setText(currentEditorText)
        self.close()
        return 0

    def handleItemClicked(self, item):
        if item.checkState() == QtCore.Qt.Checked:
            self.checked_filed_list.append(item.text())
        return 0

    def handleLogin(self):
        if (self.userName.text() == 'admin' and
                    self.userPwd.text() == '0'):
            self.accept()
        else:
            QMessageBox.warning(
                self, 'Error', 'Bad user or password')
        return self.mysql_hive_combo.currentText()


class NewFile(QDialog):
    def __init__(self, parent=None):
        super(NewFile, self).__init__(parent)
        self.setWindowTitle('new file panel')
        self.DataNameLabel = QLabel('DataBase:')
        self.DataName = QLineEdit()
        self.DataName.setText('database')
        self.Tablenamelabel = QLabel('Table:')
        self.Tablename = QLineEdit()
        self.Tablename.setText('table')
        self.Tablenumlabel = QLabel('TableNum:')
        self.Tablenum = QLineEdit()
        self.Tablenum.setText('6')
        self.mysql_hive = QLabel('Type:')
        self.mysql_hive_combo = QComboBox()
        self.mysql_hive_combo.addItem("MySQL")
        self.mysql_hive_combo.addItem("Hive")
        self.buttonSubmit = QPushButton('Submit')
        self.setWindowTitle('Submit')
        self.dictionary = QLabel("Dictionary:")
        self.texteditfile = QLineEdit()
        self.texteditfile.setText('./test.xlsx')
        self.file_location = QPushButton("···")
        self.file_location.setStyleSheet('QPushButton {font: 20px}')
        self.file_location.clicked.connect(self.getfile)
        self.buttonSubmit.clicked.connect(self.new2editor)

        self.layout = QGridLayout()
        self.layout.setSpacing(10)
        self.layout.addWidget(self.DataNameLabel, 0, 0)
        self.layout.addWidget(self.DataName, 0, 1)
        self.layout.addWidget(self.Tablenamelabel, 1, 0)
        self.layout.addWidget(self.Tablename, 1, 1)
        self.layout.addWidget(self.Tablenumlabel, 2, 0)
        self.layout.addWidget(self.Tablenum, 2, 1)
        self.layout.addWidget(self.mysql_hive, 3, 0)
        self.layout.addWidget(self.mysql_hive_combo, 3, 1)
        self.layout.addWidget(self.dictionary, 4, 0)
        self.layout.addWidget(self.texteditfile, 4, 1)
        self.layout.addWidget(self.file_location, 4, 2)
        self.layout.addWidget(self.buttonSubmit, 5, 1)

        self.setLayout(self.layout)

    def getfile(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file',
                                            '.', "excel files (*.txt *.xls *.xlsx)")
        self.texteditfile.setText(fname[0])
        return fname[0]

    def generateSQLtemplate(self):
        window.editor.clear()
        if self.mysql_hive == 'MySQL':
            self.sqlTemplate = []
            for item in range(int(self.tableNum)):
                sql = "DROP TABLE " + self.dataName + "." + self.tableName + "_" + str(item + 1) + ";\n" \
                                                                                                   "CREATE TABLE " + self.dataName + "." + self.tableName + "_" + str(
                    item + 1) + " AS\n" \
                                "SELECT * FROM\n" \
                                ";\n" \
                                "ALTER TABLE " + self.dataName + "." + self.tableName + "_" + str(
                    item + 1) + " ADD INDEX();\n"
                self.sqlTemplate.append(sql)
                window.editor.append(sql)
        # TODO hive
        return 0

    def new2editor(self):
        self.dataName, self.tableName, self.tableNum, self.mysql_hive, self.excel_addr = (
            self.DataName.text(), self.Tablename.text(), self.Tablenum.text(), self.mysql_hive_combo.currentText(),
            self.texteditfile.text())
        self.generateSQLtemplate()
        self.loadDBtoLEFT()
        self.close()

        return 0

    def handleLogin(self):
        if (self.userName.text() == 'admin' and
                    self.userPwd.text() == '0'):
            self.accept()
        else:
            QMessageBox.warning(
                self, 'Error', 'Bad user or password')
        return self.mysql_hive_combo.currentText()

    def readExcel(self):
        excelPath = self.texteditfile.text()
        d_t_list = []
        df = pd.read_excel(excelPath)
        data_table = df[['database', 'tablename']].drop_duplicates()
        self.data_table_desc_dict = {}
        for item in range(0, len(data_table.index)):
            d_t = data_table.iloc[item]['database'] + '.' + data_table.iloc[item]['tablename']
            d_t_list.append(d_t)

        for itemt in range(0, len(df.index)):
            d_tt = df.iloc[itemt]['database'] + '.' + df.iloc[itemt]['tablename']
            if d_tt in self.data_table_desc_dict.keys():
                self.data_table_desc_dict[d_tt].append(
                    (df.iloc[itemt]['Field'], df.iloc[itemt]['Type'], df.iloc[itemt]['Key']))
            else:
                self.data_table_desc_dict[d_tt] = [
                    (df.iloc[itemt]['Field'], df.iloc[itemt]['Type'], df.iloc[itemt]['Key'])]

        window.sqlDataList.clear()
        window.sqlDataList.addItems(d_t_list)
        return 0

    def loadDBtoLEFT(self):
        db = MySQLdb.connect("localhost", "root", "0", "bandwidthDB")
        cursor = db.cursor()
        self.readExcel()

        return 0


class Window(QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        self.setWindowTitle('Database IDE')
        self.setupFileMenu()
        self.setupDevelopPanel()
        self.setupSpliterPanel()
        self.setCentralWidget(self.mainSplitter)
        self.setupGeo()
        self.initSqlDataList()
        self.initMessageView()
        self.show()

    def initSqlDataList(self):
        self.sqlDataList.addItems([str(item) for item in range(1, 9)])
        return 0

    def initMessageView(self):
        self.messageView.setText('hello IDE')
        return 0

    def setupGeo(self):
        self.screen = QDesktopWidget().screenGeometry()
        self.setGeometry(0, 0, self.screen.width(), self.screen.height())
        return 0

    def showItem(self):
        sender = self.sender()
        self.data_tabel_item = sender.currentItem().text()
        self.desc_cont = self.newFile.data_table_desc_dict[self.data_tabel_item]
        view_desc = ViewDesc()
        view_desc.show()
        view_desc.exec_()

        return 0

    def setupSpliterPanel(self):
        self.sqlDataList = QListWidget()
        self.sqlDataList.itemDoubleClicked.connect(self.showItem)
        # self.messagesList = QListWidget()
        self.messageView = QTextBrowser()
        self.messageSplitter = QSplitter(Qt.Vertical)
        self.messageSplitter.addWidget(self.editor)
        self.messageSplitter.addWidget(self.messageView)
        self.mainSplitter = QSplitter(Qt.Horizontal)
        self.mainSplitter.addWidget(self.sqlDataList)
        self.mainSplitter.addWidget(self.messageSplitter)
        self.mainSplitter.setStretchFactor(0, 1)
        self.mainSplitter.setStretchFactor(1, 3)
        self.messageSplitter.setStretchFactor(0, 1)
        self.messageSplitter.setStretchFactor(1, 2)

        settings = QSettings()
        if settings.value("MainWindow/Geometry") or \
                settings.value("MainWindow/State") or \
                settings.value("MainSplitter"):
            self.restoreGeometry(
                QByteArray(settings.value("MainWindow/Geometry")))
            self.restoreState(
                QByteArray(settings.value("MainWindow/State")))
            self.messageSplitter.restoreState(
                QByteArray(settings.value("MessageSplitter")))
            self.mainSplitter.restoreState(
                QByteArray(settings.value("MainSplitter")))

        status = self.statusBar()
        status.setSizeGripEnabled(False)
        status.showMessage("Ready", 5000)
        return 0

    def setupFileMenu(self):
        fileMenu = QMenu("&File", self)
        self.menuBar().addMenu(fileMenu)

        fileMenu.addAction("&New...", self.newFile, "Ctrl+N")
        fileMenu.addAction("&Open...", self.openFile, "Ctrl+O")
        fileMenu.addAction("E&xit", QApplication.instance().quit, "Ctrl+Q")

    def setupDevelopPanel(self):
        font = QFont()
        font.setFamily('Courier')
        font.setFixedPitch(True)
        font.setPointSize(20)

        self.editor = QTextEdit()
        self.editor.setFont(font)
        self.editor.setText("""DROP""")

        self.highlighter = Highlighter(self.editor.document())

    def newFile(self):
        self.newFile = NewFile()
        self.newFile.show()
        self.newFile.exec_()

    def openFile(self):
        print('openFile')
        return 0

    def closeEvent(self, event):
        settings = QSettings()
        settings.setValue("MainWindow/Geometry",
                          self.saveGeometry())
        settings.setValue("MainWindow/State",
                          self.saveState())
        settings.setValue("MessageSplitter",
                          self.messageSplitter.saveState())
        settings.setValue("MainSplitter",
                          self.mainSplitter.saveState())


class Login(QDialog):
    def __init__(self, parent=None):
        super(Login, self).__init__(parent, )

        self.setWindowTitle('login panel')
        self.userNameLabel = QLabel('User:')
        self.userName = QLineEdit('admin')
        self.userPwdLabel = QLabel('Password:')
        self.userPwd = QLineEdit('0')
        self.userPwd.setEchoMode(QLineEdit.Password)
        self.mysql_hive = QLabel('Type:')
        self.mysql_hive_combo = QComboBox()
        self.mysql_hive_combo.addItem("MySQL")
        self.mysql_hive_combo.addItem("Hive")
        self.buttonLogin = QPushButton('Login')
        self.buttonLogin.clicked.connect(self.handleLogin)

        self.layout = QGridLayout()
        self.layout.setSpacing(10)
        self.layout.addWidget(self.userNameLabel, 0, 0)
        self.layout.addWidget(self.userName, 0, 1)
        self.layout.addWidget(self.userPwdLabel, 1, 0)
        self.layout.addWidget(self.userPwd, 1, 1)
        self.layout.addWidget(self.mysql_hive, 2, 0)
        self.layout.addWidget(self.mysql_hive_combo, 2, 1)
        # self.layout.addWidget(self.userNameLabel, 3, 0)
        self.layout.addWidget(self.buttonLogin, 3, 1)
        self.setLayout(self.layout)

    def handleLogin(self):
        if (self.userName.text() == 'admin' and
                    self.userPwd.text() == '0'):
            self.accept()
        else:
            QMessageBox.warning(
                self, 'Error', 'Bad user or password')
        return self.mysql_hive_combo.currentText()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    login = Login()

    if login.exec_() == QDialog.Accepted:
        window = Window()
        window.show()
        sys.exit(app.exec_())
