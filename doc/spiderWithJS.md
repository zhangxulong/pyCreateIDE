# Web-scraping JavaScript page with Python

[answer in stack overflow](https://stackoverflow.com/questions/8049520/web-scraping-javascript-page-with-python)

## Maybe selenium can do it.

### use webdriver 
```bash
brew install chromedriver
which chromedriver get the driver path
```
And then use it like this webdriver.Chrome("/usr/local/bin/chromedriver")


```python
from bs4 import BeautifulSoup
from selenium import webdriver
import time
url='http://www.cninfo.com.cn/cninfo-new/disclosure/szse'
driver = webdriver.Chrome()
driver.get(url)
time.sleep(5)
htmlSource = driver.page_source
print(htmlSource)
bf = BeautifulSoup(htmlSource, 'lxml')
targets = bf.find_all('a', target="_blank")
print(targets)
```
