# -*- coding:UTF-8 -*-
import os

import requests
from bs4 import BeautifulSoup

url_link = "http://news.sina.com.cn/"
if __name__ == '__main__':
    list_url = []
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"
    }
    req = requests.get(url=url_link, headers=headers)
    req.encoding = 'utf-8'
    html = req.text
    bf = BeautifulSoup(html, 'lxml')
    targets_url = bf.find_all('a', target="_blank")
    news_title_link = {}
    for url in targets_url:
        url_url = BeautifulSoup(str(url), 'lxml')
        news_title_link[url.getText()] = url_url.a.get('href')

    if 'sinaNews' not in os.listdir():
        os.makedirs('sinaNews')

    for news_title_key in news_title_link.keys():
        news_link = news_title_link[news_title_key]
        contend_req = requests.get(url=news_link, headers=headers)
        contend_req.encoding = 'utf-8'
        html = contend_req.text
        bf = BeautifulSoup(html, 'lxml')
        contend_targ = bf.find_all('div', id="artibody")
        if len(contend_targ) != 0:
            print(contend_targ)
            contend = BeautifulSoup(str(contend_targ), 'lxml')
            conted_list = contend.find_all('p')
            print(conted_list)
            txt_contend = 'Sina news contend By spider:::::' + news_link + '\n'
            for text in conted_list:
                txt_contend += ('\n' + text.getText())
            txt_file = open('sinaNews/' + news_title_key + '.txt', 'w')
            txt_file.write(txt_contend)
            txt_file.close()
